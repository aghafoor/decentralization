package com.rise.dap.api.persistance;

import java.util.List;
import java.util.Date;
import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.BindBean;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;

@RegisterMapper(DAPApplyMapper.class)
public interface DAPApplyDAO{
	@SqlUpdate("CREATE TABLE IF NOT EXISTS `DAPApply` ( `sha256` varchar(100) NOT NULL,`metaData` varchar(100) NULL DEFAULT NULL,`reqRecDate` timestamp NULL DEFAULT NULL,`reqProcessedBy` varchar(100) NULL DEFAULT NULL,`reqProcessedDate` timestamp NULL DEFAULT NULL,`roles` varchar(100) NULL DEFAULT NULL,`sign` varchar(100) NULL DEFAULT NULL,`signType` varchar(100) NULL DEFAULT NULL,`publicKey` varchar(100) NULL DEFAULT NULL,`others` varchar(100) NULL DEFAULT NULL,`publicKeyType` varchar(100) NULL DEFAULT NULL,`status` varchar(100) NULL DEFAULT NULL, PRIMARY KEY (`sha256`)) ENGINE=InnoDB DEFAULT CHARSET=latin1;")
	public void create();

	@SqlUpdate("insert into DAPApply(sha256, metaData, reqRecDate, reqProcessedBy, reqProcessedDate, roles, sign, signType, publicKey, others, publicKeyType, status) values(:sha256, :metaData, :reqRecDate, :reqProcessedBy, :reqProcessedDate, :roles, :sign, :signType, :publicKey, :others, :publicKeyType, :status)")
	public int post(@BindBean DAPApply dapapply_);

	@SqlUpdate("UPDATE DAPApply SET sha256=:sha256, metaData=:metaData, reqRecDate=:reqRecDate, reqProcessedBy=:reqProcessedBy, reqProcessedDate=:reqProcessedDate, roles=:roles, sign=:sign, signType=:signType, publicKey=:publicKey, others=:others, publicKeyType=:publicKeyType, status=:status WHERE sha256=:sha256")
	public int put(@BindBean DAPApply dapapply_);

	@SqlQuery("select * from DAPApply;")
	public List<DAPApply> list();

	@SqlQuery("select * from DAPApply where sha256=:sha256;")
	public DAPApply get(@Bind("sha256") String sha256);

	@SqlUpdate("Delete from DAPApply where sha256=:sha256;")
	public int delete(@Bind("sha256") String sha256);

}
