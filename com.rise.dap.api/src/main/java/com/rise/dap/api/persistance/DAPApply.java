package com.rise.dap.api.persistance;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
@Entity
@Table(name = "DAPApply")
@XmlRootElement
public class DAPApply{
	@Id
	@Column(name = "sha256")
	private String sha256;
	private String metaData;
	private Date reqRecDate;
	private String reqProcessedBy;
	private Date reqProcessedDate;
	private String roles;
	private String sign;
	private String signType;
	private String  publicKey;
	private String others;
	private String  publicKeyType;
	private String status;
	public DAPApply (){

	}
	public DAPApply ( String sha256,String metaData,Date reqRecDate,String reqProcessedBy,Date reqProcessedDate,String roles,String sign,String signType,String  publicKey,String others,String  publicKeyType,String status){
		this.sha256 = sha256; 
		this.metaData = metaData; 
		this.reqRecDate = reqRecDate; 
		this.reqProcessedBy = reqProcessedBy; 
		this.reqProcessedDate = reqProcessedDate; 
		this.roles = roles; 
		this.sign = sign; 
		this.signType = signType; 
		this.publicKey = publicKey; 
		this.others = others; 
		this.publicKeyType = publicKeyType; 
		this.status = status; 
	}

	public String getSha256(){
		return this.sha256;
	}
	public void setSha256(String sha256) {
		this.sha256 = sha256; 
	}
	public String getMetaData(){
		return this.metaData;
	}
	public void setMetaData(String metaData) {
		this.metaData = metaData; 
	}
	public Date getReqRecDate(){
		return this.reqRecDate;
	}
	public void setReqRecDate(Date reqRecDate) {
		this.reqRecDate = reqRecDate; 
	}
	public String getReqProcessedBy(){
		return this.reqProcessedBy;
	}
	public void setReqProcessedBy(String reqProcessedBy) {
		this.reqProcessedBy = reqProcessedBy; 
	}
	public Date getReqProcessedDate(){
		return this.reqProcessedDate;
	}
	public void setReqProcessedDate(Date reqProcessedDate) {
		this.reqProcessedDate = reqProcessedDate; 
	}
	public String getRoles(){
		return this.roles;
	}
	public void setRoles(String roles) {
		this.roles = roles; 
	}
	public String getSign(){
		return this.sign;
	}
	public void setSign(String sign) {
		this.sign = sign; 
	}
	public String getSignType(){
		return this.signType;
	}
	public void setSignType(String signType) {
		this.signType = signType; 
	}
	public String  getPublicKey(){
		return this.publicKey;
	}
	public void setPublicKey(String  publicKey) {
		this.publicKey = publicKey; 
	}
	public String getOthers(){
		return this.others;
	}
	public void setOthers(String others) {
		this.others = others; 
	}
	public String  getPublicKeyType(){
		return this.publicKeyType;
	}
	public void setPublicKeyType(String  publicKeyType) {
		this.publicKeyType = publicKeyType; 
	}
	public String getStatus(){
		return this.status;
	}
	public void setStatus(String status) {
		this.status = status; 
	}
}
