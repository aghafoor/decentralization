package com.rise.dap.api.main;

import com.rise.dap.api.persistance.*;
import com.rise.dap.api.utils.*;
import java.net.SocketException;
import java.util.Map;
import javax.sql.DataSource;
import org.glassfish.jersey.media.multipart.MultiPartFeature;
import org.skife.jdbi.v2.DBI;
import io.dropwizard.Application;
import io.dropwizard.assets.AssetsBundle;
import io.dropwizard.configuration.EnvironmentVariableSubstitutor;
import io.dropwizard.configuration.SubstitutingSourceProvider;
import io.dropwizard.db.DataSourceFactory;
import io.dropwizard.migrations.MigrationsBundle;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import io.dropwizard.views.ViewBundle;
import io.federecio.dropwizard.swagger.SwaggerBundle;
import io.federecio.dropwizard.swagger.SwaggerBundleConfiguration;


public class DAPApplyApplication extends Application<DAPApplyConfiguration> {


	public DAPApplyApplication(){
		
	}

	@Override
	public String getName() {
		return "DAPApply";
	}

	public static void main(String[] args) throws Exception {

		new DAPApplyApplication().run(args);
	}

	@Override
	public void initialize(Bootstrap<DAPApplyConfiguration> bootstrap){
		bootstrap.setConfigurationSourceProvider(new SubstitutingSourceProvider(
			bootstrap.getConfigurationSourceProvider(), new EnvironmentVariableSubstitutor(false)));
		bootstrap.addBundle(new AssetsBundle());
		bootstrap.addBundle(new MigrationsBundle<DAPApplyConfiguration>(){
			@Override
			public DataSourceFactory getDataSourceFactory(DAPApplyConfiguration configuration){
				return configuration.getDataSourceFactory();
			}
		});
		bootstrap.addBundle(new ViewBundle<DAPApplyConfiguration>(){
			@Override
			public Map<String, Map<String, String>> getViewConfiguration(DAPApplyConfiguration configuration) {
				return configuration.getViewRendererConfiguration();
			}
		});
		bootstrap.addBundle(new SwaggerBundle<DAPApplyConfiguration>(){
			@Override
			public SwaggerBundleConfiguration getSwaggerBundleConfiguration(DAPApplyConfiguration configuration){
			return configuration.getSwaggerBundleConfiguration();
			}
		});
	}


	private static final String SQL = "sql";

	@Override
	public void run(DAPApplyConfiguration configuration, Environment environment) throws SocketException {
		final DataSource dataSource = configuration.getDataSourceFactory().build(environment.metrics(), SQL);
		DBI dbi = new DBI(dataSource);
		DAPApplyService dapapplyService = dbi.onDemand(DAPApplyService.class);
		dapapplyService.create();
		DAPApplyResource dapapplyResource = new DAPApplyResource(dapapplyService);
		final TemplateHealthCheck healthCheck = new TemplateHealthCheck(configuration.getTemplate());
		environment.healthChecks().register("template", healthCheck);
		environment.jersey().register(MultiPartFeature.class);
		environment.jersey().register(dapapplyResource);
	}


}
