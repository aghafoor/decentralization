package com.rise.dap.api.persistance;


import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

public class DAPApplyMapper implements ResultSetMapper<DAPApply> {

	private static final String  sha256 = "sha256";
	private static final String  metaData = "metaData";
	private static final String  reqRecDate = "reqRecDate";
	private static final String  reqProcessedBy = "reqProcessedBy";
	private static final String  reqProcessedDate = "reqProcessedDate";
	private static final String  roles = "roles";
	private static final String  sign = "sign";
	private static final String  signType = "signType";
	private static final String  publicKey = "publicKey";
	private static final String  others = "others";
	private static final String  publicKeyType = "publicKeyType";
	private static final String  status = "status";

	public DAPApply map(int i, ResultSet resultSet, StatementContext statementContext) throws SQLException {
		return new DAPApply(resultSet.getString(sha256), resultSet.getString(metaData), resultSet.getTimestamp(reqRecDate), resultSet.getString(reqProcessedBy), resultSet.getTimestamp(reqProcessedDate), resultSet.getString(roles), resultSet.getString(sign), resultSet.getString(signType), resultSet.getString(publicKey), resultSet.getString(others), resultSet.getString(publicKeyType), resultSet.getString(status));

	}
}
