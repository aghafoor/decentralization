package com.ri.se.dap.test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.Signature;
import java.security.spec.ECGenParameterSpec;
import java.security.spec.ECPublicKeySpec;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;

import org.bouncycastle.jce.ECNamedCurveTable;
import org.bouncycastle.jce.ECPointUtil;
import org.bouncycastle.jce.interfaces.ECPublicKey;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.jce.spec.ECNamedCurveParameterSpec;
import org.bouncycastle.jce.spec.ECNamedCurveSpec;
import org.web3j.utils.Numeric;

public class ECTest {

	/**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws Exception {
        
    	boolean bo = true;
    	
    	genPublic();
    	if(bo) {
    		return;
    	}
    	
    	
        KeyPairGenerator keyGen = KeyPairGenerator.getInstance("EC");

        keyGen.initialize(new ECGenParameterSpec("secp256r1"), new SecureRandom());

        KeyPair pair = keyGen.generateKeyPair();
        
        PrivateKey priv = pair.getPrivate();
        PublicKey pub = pair.getPublic();

        
        byte[] signed = sign(priv , "String message");
        System.out.println(verify(pub , signed, "String message"));
        
        /*
         * Create a Signature object and initialize it with the private key
         */

        Signature ecdsa = Signature.getInstance("SHA256withECDSA");

        ecdsa.initSign(priv);

        String str = "This is string to sign";
        byte[] strByte = str.getBytes("UTF-8");
        ecdsa.update(strByte);

        /*
         * Now that all the data to be signed has been read in, generate a
         * signature for it
         */

        byte[] realSig = ecdsa.sign();
        System.out.println("Signature: " + Numeric.toHexString(realSig));
    }
    
    
    
    
    public static byte[] sign(PrivateKey privateKey, String message) throws Exception {
        Signature signature = Signature.getInstance("SHA256withECDSA");
        signature.initSign(privateKey);

        signature.update(message.getBytes());

        return signature.sign();
    }

    public static boolean verify(PublicKey publicKey, byte[] signed, String message) throws Exception {
        Signature signature = Signature.getInstance("SHA256withECDSA");
        signature.initVerify(publicKey);

        signature.update(message.getBytes());

        return signature.verify(signed);
    }
    
    public static void genPublic() throws Exception {
    	   
    	
    	 KeyPairGenerator keyGen = KeyPairGenerator.getInstance("EC");

         keyGen.initialize(new ECGenParameterSpec("secp256r1"), new SecureRandom());

         KeyPair pair = keyGen.generateKeyPair();
         
         PrivateKey priv = pair.getPrivate();
         PublicKey pubOld = pair.getPublic();
         PublicKey pub = getPublicKeyFromBytes(pubOld.getEncoded());
         
         byte[] signed = sign(priv , "String message");
         
    	 System.out.println(verify(pub , signed, "String message"));
    	 
    }
    private static PublicKey getPublicKeyFromBytes(byte[] pubKey) throws NoSuchAlgorithmException, InvalidKeySpecException {
        ECNamedCurveParameterSpec spec = ECNamedCurveTable.getParameterSpec("secp256r1");
        KeyFactory kf = KeyFactory.getInstance("EC", new BouncyCastleProvider());
        ECNamedCurveSpec params = new ECNamedCurveSpec("secp256r1", spec.getCurve(), spec.getG(), spec.getN());
        java.security.spec.ECPoint point =  ECPointUtil.decodePoint(params.getCurve(), pubKey);
        ECPublicKeySpec pubKeySpec = new ECPublicKeySpec(point, params);
        ECPublicKey pk = (ECPublicKey) kf.generatePublic(pubKeySpec);
        return pk;
    }
    
    public String toBase64String(PublicKey publicKey){
    	ByteArrayOutputStream baos = new ByteArrayOutputStream();
    	PrintStream out = new PrintStream(baos );
    	out.println(Base64.getEncoder().encodeToString(publicKey.getEncoded()));
    	out.close();
    	return new String(baos.toByteArray());
    	}

    	public PublicKey fromBase64StringToPublicKey(String publicKeyEncoded) throws Exception{
    	byte []pKey = Base64.getDecoder().decode(publicKeyEncoded);
    	try {
    	PublicKey publicKey =
    	   KeyFactory.getInstance("RSA").generatePublic(new X509EncodedKeySpec(pKey));
    	return publicKey;
    	} catch (InvalidKeySpecException | NoSuchAlgorithmException e) {
    	throw new Exception(e);
    	}
    	}
    
    
}