package com.ri.se.dap.test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Hashtable;
import java.util.stream.Stream;

public class GanacheAccountData {

	public Hashtable<String, String> publicKey = new Hashtable<String, String>();
	public Hashtable<String, String> privateKey = new Hashtable<String, String>();

	public static void main(String[] args) throws IOException {
	}

	public GanacheAccountData(String file) {
		System.out.println(file);
		try {
			try (Stream<String> stream = Files.lines(Paths.get(file))) {
				int previous = 0;
				stream.filter(s -> s.startsWith("(")).forEach(s -> {
					int index = s.indexOf(")");
					if (index != -1) {
						String i = s.substring(1, index);
						if (publicKey.containsKey(i)) {
							String key = s.substring(index + 1).trim();
							privateKey.put(i, key);
						} else {
							int zIn = s.indexOf(" ");
							String temp = s.substring(zIn);
							temp = temp.trim();
							int nindex = temp.indexOf(" ");
							String key = temp.substring(0, nindex).trim();
							publicKey.put(i, key);
						}
					}
				});
			}
			System.out.println(publicKey.get("0"));
			System.out.println(privateKey.get("0"));
		} catch (IOException io) {
			io.printStackTrace();
		}
	}

	public IdentityData getExampleIdentity(int number) {
		IdentityData exampleIdentity = new IdentityData();
		exampleIdentity.setPrivateKey(privateKey.get(number+""));
		return exampleIdentity;
	}
	
	public String [] getRestAddresses() {
		String[] other = new String[6];
		for(int i=4; i < 10; i++) {
			other [i-4] = publicKey.get(i+"");
		}
		return other;
	}
}