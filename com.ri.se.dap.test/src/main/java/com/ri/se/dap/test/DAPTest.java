package com.ri.se.dap.test;

import java.math.BigInteger;
import java.util.List;
import java.util.Objects;

import javax.swing.JComboBox;
import javax.swing.JOptionPane;

import org.web3j.crypto.Credentials;
import org.web3j.protocol.Web3j;
import org.web3j.protocol.core.methods.response.EthAccounts;
import org.web3j.protocol.http.HttpService;
import org.web3j.utils.Convert.Unit;
import org.web3j.utils.Numeric;

import com.ri.se.dap.AccountsManager;
import com.ri.se.dap.Vblock;
import com.ri.se.dap.VeidblockManager;
import com.ri.se.dap.VeidblockSmartContract;
import com.ri.se.dap.Web3JConnector;

public class DAPTest {

	private static Web3j getWeb3j(String url) throws Exception {
		if (Objects.isNull(url)) {
			System.err.print("URL is not defined, please call constructor once before geting web3j instance !");
			throw new Exception("URL is not defined, please call constructor once before geting web3j instance !");
		}
		return Web3j.build(new HttpService(url));
	}

	public static void main(String[] args) throws Exception {
		//GanacheAccountData ganacheAccountData = new GanacheAccountData("/home/blockchain/eth/store.txt");
		//String token = "1111222233334444555566667777888811112222333344445555666677778888111122223333444455556666777788881111222233334444555566667777888811112222333344445555666677778888111122223333444455556666777788881111222233334444555566667777888811112222333344445555666677778888";
		IdentityData supper = new IdentityData();
		supper.setUsername("anders");
		supper.setPassword("gavler");
		String sc = new DAPTest().contractManager(supper);
		new DAPTest().dapTest(sc, supper);
	}

	
	public String contractManager(IdentityData supper) throws Exception {

		DAPTest manageEther = new DAPTest();
		EtherProps etherProps = new EtherProps();
		String[] optionsStr = { "Manage Balanace and Deploy Contract", "Manage Balance", "Deploy Contract" };
		JComboBox jcd = new JComboBox(optionsStr);
		jcd.setEditable(false);
		String walletDir = etherProps.getCredentialsPath();
		Object[] options = new Object[] {};
		String option = JOptionPane.showInputDialog(null, "Please slection options ?", "Smart Contract Manager",
				JOptionPane.QUESTION_MESSAGE, null, optionsStr, optionsStr[0]).toString();
		if (option.equalsIgnoreCase(optionsStr[0])) {
			manageEther.manageAccounts(0, "9000000000", walletDir, supper.getUsername(), supper.getPassword(),
					etherProps.getEtherURL());
			VeidblockSmartContract veidblockSmartContract = new VeidblockSmartContract();
			String address = new AccountsManager().createAccount(walletDir, supper.getUsername(),
					supper.getPassword());
			Credentials cred = new AccountsManager().getCredentials(walletDir, supper.getUsername(),
					supper.getPassword());
			Web3JConnector web3JConnector = new Web3JConnector(etherProps.getEtherURL());
			String smartContractAddress = veidblockSmartContract.deployContract(cred, web3JConnector);
			System.out.println("Owner of the smart contract : "+cred.getAddress());
			System.out.println("This is smart contract address : " + smartContractAddress);
			manageEther.displayMyBalanace(walletDir, supper.getUsername(), supper.getPassword(),
					etherProps.getEtherURL());
			return smartContractAddress ;
		} else if (option.equalsIgnoreCase(optionsStr[1])) {
			manageEther.manageAccounts(0, "9000000000", walletDir, supper.getUsername(), supper.getPassword(),
					etherProps.getEtherURL());
		} else {
			if (option.equalsIgnoreCase(optionsStr[2])) {
				VeidblockSmartContract veidblockSmartContract = new VeidblockSmartContract();
				String address = new AccountsManager().createAccount(walletDir, supper.getUsername(),
						supper.getPassword());
				Credentials cred = new AccountsManager().getCredentials(walletDir, supper.getUsername(),
						supper.getPassword());
				Web3JConnector web3JConnector = new Web3JConnector(etherProps.getEtherURL());
				System.out.println("Owner of the smart contract : "+cred.getAddress());
				String smartContractAddress = veidblockSmartContract.deployContract(cred, web3JConnector);
				System.out.println("This is smart contract address : " + smartContractAddress);
				manageEther.displayMyBalanace(walletDir, supper.getUsername(), supper.getPassword(),
						etherProps.getEtherURL());
				return smartContractAddress;
			}
		}
	
		return null;
	}
	
	public void dapTest(String sc, IdentityData supper ) throws Exception {
		
		GanacheAccountData ganacheAccountData = new GanacheAccountData("/home/blockchain/eth/store.txt");
		String token = "1111222233334444555566667777888811112222333344445555666677778888111122223333444455556666777788881111222233334444555566667777888811112222333344445555666677778888111122223333444455556666777788881111222233334444555566667777888811112222333344445555666677778888";
		String walletDir = "/home/blockchain/eth/dapp/creddap/";
		String url = "http://localhost:8545";
		
		DAPTest manageEther = new DAPTest();
		
		/*
		 * IdentityData supper = new
		 * IdentityData();//ganacheAccountData.getExampleIdentity(0);
		 * supper.setUsername("anders"); supper.setPassword("gavler");
		 */String addr = new AccountsManager().createAccount(walletDir, supper.getUsername(),supper.getPassword());
		supper.setPublicKey(addr);
		System.out.println("Owner of the smart contract 2 : "+addr);
		
		manageEther.manageAccounts(0, "9000000000", walletDir, supper.getUsername(), supper.getPassword(),url);
		
		IdentityData admin = ganacheAccountData.getExampleIdentity(1);
		admin.setUsername("peter");
		admin.setPassword("altmann"); 
		addr = new AccountsManager().createAccount(walletDir, admin.getUsername(),admin.getPassword());
		admin.setPublicKey(addr);
		admin.setPrivateKey(Numeric.toHexStringWithPrefix(new AccountsManager().getCredentials(walletDir, admin.getUsername(),admin.getPassword()).getEcKeyPair().getPrivateKey()));
		
		manageEther.manageAccounts(0, "9000000000", walletDir, admin.getUsername(), admin.getPassword(),url);
		
		IdentityData abdul = ganacheAccountData.getExampleIdentity(2);
		abdul.setUsername("abdul");
		abdul.setPassword("ghafoor"); 
		abdul.setToken("thisisabdul");
		addr = new AccountsManager().createAccount(walletDir, abdul.getUsername(),abdul.getPassword());
		abdul.setPublicKey(addr);
		
		IdentityData sugandh = ganacheAccountData.getExampleIdentity(2);
		sugandh.setUsername("Sugandh");
		sugandh.setPassword("Sahna"); 
		sugandh.setToken("sugandhtoken");
		addr = new AccountsManager().createAccount(walletDir, sugandh.getUsername(),sugandh.getPassword());
		sugandh.setPublicKey(addr);
		
		IdentityData andersL = ganacheAccountData.getExampleIdentity(2);
		andersL.setUsername("andersL");
		andersL.setPassword("Lindgren");
		andersL.setToken("andersLToken");
		addr = new AccountsManager().createAccount(walletDir, andersL.getUsername(),andersL.getPassword());
		andersL.setPublicKey(addr);
		
		System.out.println(new VeidblockManager(url).giveAdminRights(sc, walletDir, supper.getUsername(), supper.getPassword(), admin.getPublicKey()));
		System.out.println(new VeidblockManager(url).giveUserRights(sc, admin.getPrivateKey(),
				abdul.getPublicKey(), abdul.getToken()));
		IdentityData[] add = new IdentityData[2];
		add[0] = sugandh;
		add[1] = andersL;
		
		for (int i = 0; i < add.length; i++) {
			System.out.println(new VeidblockManager(url).giveUserRights(sc, admin.getPrivateKey(), add[i].getPublicKey(),
					add[i].getToken()));
		}
		flowOfIntructionsToCommunicateWithSC_2(add[0], sc);
	}
	
	public static void flowOfIntructionsToCommunicateWithSC_2(IdentityData abdul, String smartContract) throws Exception {
		String url = "http://localhost:8545";
		Web3JConnector web3JConnector = new Web3JConnector(url);
		double bal = new EtherTransferManager().getEthBalance(web3JConnector.getWeb3j(), abdul.getPublicKey());
		System.out.printf("Admin Account (%s) balanace = %.0f\n", abdul.getPublicKey(), bal, Unit.ETHER.toString());
		Vblock veidblockAdmin = new VeidblockSmartContract().getContract(smartContract,
				abdul.getPrivateKey(), web3JConnector);
		try {
			//0xe0801c7eb39f6f6135fcf3d5f0ccd91cc294d79c
			//0xe0801c7eb39f6f6135fcf3d5f0ccd91cc294d79c
			boolean ss = veidblockAdmin.isAdmin(abdul.getPublicKey()).send();
			System.out.println("Are you '" + abdul.getUsername() + "' Admin : " + ss);
			BigInteger s = veidblockAdmin.totalAdmins().send();
			System.out.println("Total Admin : " + s.doubleValue());
			s = veidblockAdmin.totalUsers().send();
			System.out.println("Total Users : " + s.doubleValue());
			boolean bool = veidblockAdmin.verify(abdul.getToken()).send();
			System.out.println(bool);
			String b = veidblockAdmin.publickey(abdul.getToken()).send();
			System.out.println(b);
			String owner = veidblockAdmin.owner().send();
			System.out.println(owner);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	

	public void displayMyBalanace(String walletDir, String username, String password, String etherURL)
			throws Exception {
		Credentials cred = new AccountsManager().getCredentials(walletDir, username, password);
		double bal = new EtherTransferManager().getEthBalance(getWeb3j(etherURL), cred.getAddress());
		System.out.printf("Account (%s) balanace = %.0f %s\n", cred.getAddress(), bal, Unit.ETHER.toString());
	}

	public boolean manageAccounts(int accountIndex, String money, String walletDir, String username, String password,
			String etherURL) throws Exception {
		IdentityData exampleIdentity = new GanacheAccountData(new EtherProps().getStorePath())
				.getExampleIdentity(accountIndex);
		String privateKey = exampleIdentity.getPrivateKey();// "0x5fee56d55e4909b1e6e39eb28228af1873b70a55c089e341d7cbcfed9f5486d7";
		String contractAddress = null;

		// Fetch Accounts from ganache-cli
		EthAccounts eaccts = new AccountsManager().getEthAccounts(getWeb3j(etherURL));
		List<String> aaa = eaccts.getAccounts();

		if (accountIndex + 1 > aaa.size()) {
			throw new Exception("Account does not exisits. All accounts are consumed !");
		}
		System.out.println("Total default accounts of ganash : " + aaa.size());
		// Create my own local accound
		System.out.println("Creating a new account !");
		String address = new AccountsManager().createAccount(walletDir, username, password);
		System.out.println("Address of newly created account is = " + address);
		Credentials cred = new AccountsManager().getCredentials(walletDir, username, password);

		double bal = new EtherTransferManager().getEthBalance(getWeb3j(etherURL), cred.getAddress());
		System.out.printf("Account (%s) balanace = %.0f %s\n", address, bal, Unit.ETHER.toString());

		Credentials credSender = new AccountsManager().createCredentilsFromPrivateKey(privateKey);
		double balSender = new EtherTransferManager().getEthBalance(getWeb3j(etherURL), credSender.getAddress());
		System.out.printf("Account (%s) balanace = %.0f %s\n", credSender.getAddress(), balSender,
				Unit.ETHER.toString());

		try {
			System.out.println("Transferring " + money + " " + Unit.ETHER.toString());
			System.out.println("\tfrom " + credSender.getAddress());
			System.out.println("\tto  " + cred.getAddress() + " account ... ");
			new EtherTransferManager().transferEther(getWeb3j(etherURL), privateKey, cred.getAddress(),
					money/* "4500000000" */, Unit.ETHER, "1", Unit.GWEI);
			bal = new EtherTransferManager().getEthBalance(getWeb3j(etherURL), cred.getAddress());
			System.out.printf("Updated account (%s) balanace = %.0f %s\n", address, bal, Unit.ETHER.toString());
			return true;
		} catch (Exception ibe) {
			if (ibe.getMessage().startsWith("Insuficent balance in")) {
				System.err.println("Trying another account !");
				accountIndex++;
				manageAccounts(accountIndex, money, walletDir, username, password, etherURL);
			}
			return false;
		}
	}
}
