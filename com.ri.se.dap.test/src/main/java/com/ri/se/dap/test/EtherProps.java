package com.ri.se.dap.test;

public class EtherProps {

	private String credentialsPath = "/home/blockchain/eth/dapp/creddap/";
	private String etherURL = "http://localhost:8545";
	private String storePath = "/home/blockchain/eth/store.txt";
	public String getCredentialsPath() {
		return credentialsPath;
	}
	public void setCredentialsPath(String credentialsPath) {
		this.credentialsPath = credentialsPath;
	}
	public String getEtherURL() {
		return etherURL;
	}
	public void setEtherURL(String etherURL) {
		this.etherURL = etherURL;
	}
	public String getStorePath() {
		return this.storePath;
	}
	public void setStorePath(String storePath) {
		this.storePath = storePath;
	}
	
	
	
}
