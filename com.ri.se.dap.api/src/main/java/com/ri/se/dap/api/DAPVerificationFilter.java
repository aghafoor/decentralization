package com.ri.se.dap.api;

import java.io.IOException;

import javax.annotation.Priority;
import javax.ws.rs.Priorities;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.PreMatching;
import javax.ws.rs.core.Response;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.dropwizard.auth.AuthFilter;

@PreMatching
@Priority(Priorities.AUTHENTICATION)
public class DAPVerificationFilter extends AuthFilter {

	final static Logger logger = LoggerFactory.getLogger(DAPVerificationFilter.class);

	public DAPVerificationFilter() {
		
	}

	public void filter(ContainerRequestContext requestContext) throws IOException {
		try {
			requestContext.getHeaders().add("owner", "abdul");
		} catch (Exception e) {
			throw new WebApplicationException(
					Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(null).build());
		}
	}
}