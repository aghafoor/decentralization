package com.ri.se.dap.api;

import java.util.EnumSet;
import java.util.Map;

import javax.servlet.DispatcherType;
import javax.servlet.FilterRegistration;
import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.eclipse.jetty.servlets.CrossOriginFilter;
import org.glassfish.jersey.media.multipart.MultiPartFeature;
import org.skife.jdbi.v2.DBI;

import io.dropwizard.Application;
import io.dropwizard.assets.AssetsBundle;
import io.dropwizard.auth.AuthDynamicFeature;
import io.dropwizard.configuration.EnvironmentVariableSubstitutor;
import io.dropwizard.configuration.SubstitutingSourceProvider;
import io.dropwizard.db.DataSourceFactory;
import io.dropwizard.migrations.MigrationsBundle;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import io.dropwizard.views.ViewBundle;
import io.federecio.dropwizard.swagger.SwaggerBundle;
import io.federecio.dropwizard.swagger.SwaggerBundleConfiguration;

public class DAPMain extends Application<DAPConfiguration> {

	private static final Logger logger_ = Logger.getLogger(DAPMain.class);

	private static String myURL = null;

	public DAPMain() {
	}

	public static void main(String[] args) throws Exception {
		new DAPMain().run(args);
	}

	/*
	 * private final HibernateBundle<BixIdmsDocsConfiguration> hibernateBundle = new
	 * HibernateBundle<BixIdmsDocsConfiguration>(
	 * ImmutableList.of(BIXDocument.class), new SessionFactoryFactory()) {
	 * 
	 * @Override public DataSourceFactory
	 * getDataSourceFactory(BixIdmsDocsConfiguration configuration) { return
	 * configuration.getDataSourceFactory(); } };
	 */

	@Override
	public String getName() {
		return "DAP";
	}

	@Override
	public void initialize(Bootstrap<DAPConfiguration> bootstrap) {
		// Enable variable substitution with environment variables
		bootstrap.setConfigurationSourceProvider(new SubstitutingSourceProvider(
				bootstrap.getConfigurationSourceProvider(), new EnvironmentVariableSubstitutor(false)));

		bootstrap.addBundle(new AssetsBundle());
		bootstrap.addBundle(new MigrationsBundle<DAPConfiguration>() {
			public DataSourceFactory getDataSourceFactory(DAPConfiguration configuration) {
				return configuration.getDataSourceFactory();
			}
		});
		// bootstrap.addBundle(hibernateBundle);
		bootstrap.addBundle(new ViewBundle<DAPConfiguration>() {
			@Override
			public Map<String, Map<String, String>> getViewConfiguration(DAPConfiguration configuration) {
				return configuration.getViewRendererConfiguration();
			}
		});

		bootstrap.addBundle(new SwaggerBundle<DAPConfiguration>() {
			@Override
			public SwaggerBundleConfiguration getSwaggerBundleConfiguration(DAPConfiguration configuration) {
				return configuration.getSwaggerBundleConfiguration();
			}
		});
	}

	private static final String SQL = "sql";

	@Override
	public void run(DAPConfiguration configuration, Environment environment) {

		// final BIXDocumentDAO idLedgerDao = new
		// BIXDocumentDAO(hibernateBundle.getSessionFactory());

		// final Template template = configuration.buildTemplate();

		enableCORS(environment);
		final DataSource dataSource = configuration.getDataSourceFactory().build(environment.metrics(), SQL);
		DBI dbi = new DBI(dataSource);
		// DocumentService documentService = dbi.onDemand(DocumentService.class);
		// documentService.create();

		DAPResource idmsDocsResource = new DAPResource();

		// environment.healthChecks().register("template", new
		// TemplateHealthCheck(template));

		// environment.jersey().register(DateRequiredFeature.class);
		environment.jersey().register(MultiPartFeature.class);

		environment.jersey().register(idmsDocsResource);
		DAPVerificationFilter verificationRedirection = new DAPVerificationFilter();
		environment.jersey().register(new AuthDynamicFeature(verificationRedirection));

	}

	private void enableCORS(Environment environment) {
		FilterRegistration.Dynamic filter = environment.servlets().addFilter("CORS", CrossOriginFilter.class);
		filter.setInitParameter(CrossOriginFilter.ALLOWED_METHODS_PARAM, "GET,PUT,POST,DELETE,OPTIONS");
		filter.setInitParameter(CrossOriginFilter.ALLOWED_ORIGINS_PARAM, "*");
		filter.setInitParameter(CrossOriginFilter.ACCESS_CONTROL_ALLOW_ORIGIN_HEADER, "*");
		filter.setInitParameter(CrossOriginFilter.ALLOWED_HEADERS_PARAM,
				"Content-Type,Authorization,X-Requested-With,Content-Length,Accept,Origin");
		filter.addMappingForUrlPatterns(EnumSet.allOf(DispatcherType.class), true, "/*");
	}
}
