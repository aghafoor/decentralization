package com.ri.se.dap.api;

import java.util.Date;

public class DAPRequest {
	
	private String publicKeyEC; // EC public key of the account being requested 
	private String sha256; 		// may be the root hash of merkle tree and it will be treated as a message  
	private String ecdsa; 		//(signature of sha256|others)
	private String metaData; 	// Schema of the values which were used to create sha256
	private String others; 		// may be used for future if it is null then consider empty string
	private Date reqProcessedBy;// Public key of the admin
	
	public String getPublicKeyEC() {
		return publicKeyEC;
	}
	public void setPublicKeyEC(String publicKeyEC) {
		this.publicKeyEC = publicKeyEC;
	}
	public String getSha256() {
		return sha256;
	}
	public void setSha256(String sha256) {
		this.sha256 = sha256;
	}
	public String getEcdsa() {
		return ecdsa;
	}
	public void setEcdsa(String ecdsa) {
		this.ecdsa = ecdsa;
	}
	public String getMetaData() {
		return metaData;
	}
	public void setMetaData(String metaData) {
		this.metaData = metaData;
	}
	public String getOthers() {
		return others;
	}
	public void setOthers(String others) {
		this.others = others;
	}
	public Date getReqProcessedBy() {
		return reqProcessedBy;
	}
	public void setReqProcessedBy(Date reqProcessedBy) {
		this.reqProcessedBy = reqProcessedBy;
	}

}
