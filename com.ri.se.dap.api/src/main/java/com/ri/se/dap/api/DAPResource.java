package com.ri.se.dap.api;

import java.io.IOException;

import javax.annotation.security.RolesAllowed;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@Path("/v1/dap")
@Api(value = "/dap", description = "Decentralized Authetication Protocol")
public class DAPResource {

	private static final Logger logger_ = Logger.getLogger(DAPResource.class);

	public DAPResource() {

	}

	@Context
	protected HttpServletResponse response;
	@Context
	protected HttpHeaders headers;
	
	@POST
	@Path("/admin")
	@RolesAllowed("")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Assign Admin right"
			+ "s to the received public key !", notes = "Assign Admin rights to the received public key holder !", response = String.class)
	@ApiResponses({ @ApiResponse(code = 200, message = "OK"),
			@ApiResponse(code = 500, message = "Internal Server error !"),
			@ApiResponse(code = 401, message = "Unauthorized access !") })
	public Response assignAdminRights(@Valid DAPRequest regRequest) {
		
		String publicKey = regRequest.getPublicKeyEC();
		
		/*
		 * String owner = headers.getHeaderString("owner"); if (Objects.isNull(owner)) {
		 * throw new
		 * WebApplicationException("You are not authorized to perform this action !"); }
		 */
		
		return null;
	}
	
	@POST
	@Path("/user")
	@RolesAllowed("")
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Assign user right"
			+ "s to the received public key !", notes = "Assign User rights to the received public key holder !", response = String.class)
	@ApiResponses({ @ApiResponse(code = 200, message = "OK"),
			@ApiResponse(code = 500, message = "Internal Server error !"),
			@ApiResponse(code = 401, message = "Unauthorized access !") })
	public Response assignUserRights(@Valid DAPRequest regRequest) throws IOException {
		String publicKey = regRequest.getPublicKeyEC();
		/*
		 * String owner = headers.getHeaderString("owner"); if (Objects.isNull(owner)) {
		 * throw new
		 * WebApplicationException("You are not authorized to perform this action !"); }
		 */
		return null;

	}
	
}
