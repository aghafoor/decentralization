package com.ri.se.dap;

import java.io.File;
import java.util.Objects;
import java.util.concurrent.ExecutionException;

import org.apache.log4j.Logger;
import org.web3j.crypto.Credentials;
import org.web3j.crypto.WalletUtils;
import org.web3j.protocol.Web3j;
import org.web3j.protocol.core.methods.response.EthAccounts;

public class AccountsManager {

	private static Logger logger_ = Logger.getLogger(AccountsManager.class);

	public String createAccount(String walletDir, String username, String password) throws Exception {
		try {
			
			if(!walletDir.endsWith(File.separator)) {
				walletDir =walletDir+File.separator;
			}
			logger_.error("Problems when serializing (json) RecordStatusList object !");
			String path = "";
			String userDir = walletDir + username;
			if(!new File(userDir).exists()) {
				new File(userDir).mkdirs();
			}
			if (Objects.isNull(new File(userDir).list()) || new File(userDir).list().length == 0) {
				logger_.debug("Creating new credentials !");
				path = WalletUtils.generateNewWalletFile(password, new File(userDir), true);
			} else {
				logger_.debug("Credentials already exisit !");
				File fl[] = new File(userDir).listFiles();
				for (File f : fl) {
					if (f.getName().endsWith(".json")) {
						path = f.getName();
					}
				}
			}
			Credentials credentials = WalletUtils.loadCredentials(password, userDir + "/" + path);
			return credentials.getAddress();
		} catch (Exception e) {
			logger_.error("Problems when creating credentials !");
			logger_.error(e.getMessage());
			throw new Exception(e);
		}
	}

	public Credentials getCredentials(String walletDir, String username, String password) throws Exception {
		try {
			if(!walletDir.endsWith(File.separator)) {
				walletDir =walletDir+File.separator;
			}
			String path = "";
			String userDir = walletDir + username;
			if (Objects.isNull(new File(userDir).list()) || new File(userDir).list().length == 0) {
				logger_.debug("Credentials do not exist !");
				throw new Exception("Credentials do not exist !");
			} else {
				logger_.debug("Credentials already exisit in the wallet !");
				File fl[] = new File(userDir).listFiles();
				for (File f : fl) {
					if (f.getName().endsWith(".json")) {
						path = f.getName();
					}
				}
			}
			return WalletUtils.loadCredentials(password, userDir + "/" + path);
		} catch (Exception e) {
			logger_.error("Problems when extracting (creating) credentials !");
			logger_.error(e.getMessage());
			throw new Exception(e);
		}
	}

	public Credentials createCredentilsFromPrivateKey(String privateKey) {
		Credentials credentials = Credentials.create(privateKey);// );
		return credentials;
	}

	public EthAccounts getEthAccounts(Web3j web3) throws Exception {
		EthAccounts result = new EthAccounts();
		try {
			result = web3.ethAccounts().sendAsync().get();
		} catch (InterruptedException | ExecutionException e) {
			logger_.error("Problems when fetching accounts from etherum !");
			logger_.error(e.getMessage());
			throw new Exception(e);
		}
		return result;
	}

	public boolean exists(String walletDir, String username) throws Exception {
		try {
			if(!walletDir.endsWith(File.separator)) {
				walletDir =walletDir+File.separator;
			}
			String userDir = walletDir + username;
			if (Objects.isNull(new File(userDir).list()) || new File(userDir).list().length == 0) {
				return false;
			} else {
				return true;
			}
		} catch (Exception e) {
			logger_.error("Problems when creating credentials !");
			logger_.error(e.getMessage());
			throw new Exception(e);
		}
	}
}
