package com.ri.se.dap;

import org.web3j.crypto.Credentials;
import org.web3j.tx.Contract;
import org.web3j.tx.ManagedTransaction;

import com.ri.se.dap.exceptions.EtherGenericException;

public class VeidblockSmartContract {
	public String deployContract(Credentials credentials, Web3JConnector web3JConnector) throws EtherGenericException {
		Vblock contract;
		try {
			contract = Vblock.deploy(web3JConnector.getWeb3j(), credentials, ManagedTransaction.GAS_PRICE, Contract.GAS_LIMIT).send();
			/*
			 * contract = Veidblock.deploy(web3, credentials, new DefaultGasProvider())
			 * .send();
			 */
			return contract.getContractAddress();
		} catch (Exception e) {
			throw new EtherGenericException(e);
		}
	}

	public Vblock getContract(String contractAddress, String walletDir, String username, String password, Web3JConnector web3JConnector)
			throws EtherGenericException {
		try {
			// ContractGasProvider contractGasProvider = new DefaultGasProvider();

			Vblock veidblock = Vblock.load(contractAddress, web3JConnector.getWeb3j(), new AccountsManager().getCredentials(walletDir, username, password),
					ManagedTransaction.GAS_PRICE, Contract.GAS_LIMIT);
			return veidblock;

		} catch (Exception e) {
			e.printStackTrace();
			throw new EtherGenericException(e);
		}
	}

	public Vblock getContract(String contractAddress, String privateKey, Web3JConnector web3JConnector) throws EtherGenericException {
		try {
			// ContractGasProvider contractGasProvider = new DefaultGasProvider();

			Vblock veidblock = Vblock.load(contractAddress, web3JConnector.getWeb3j(), new AccountsManager().createCredentilsFromPrivateKey(privateKey),
					ManagedTransaction.GAS_PRICE, Contract.GAS_LIMIT);
			return veidblock;

		} catch (Exception e) {
			throw new EtherGenericException(e);
		}
	}
}
